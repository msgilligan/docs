# Validating Lightning Signer

Improving Lightning security with fully validated remote signing.

You can go directly to the [code repository for VLS](https://gitlab.com/lightning-signer/validating-lightning-signer).

You can also go to the [VLS website](https://vls.tech/).

## Motivation

[Lightning nodes are in effect hot
wallets](https://medium.com/@devrandom/securing-lightning-nodes-39410747734b?)
with substantial balances that must stay on-chain to provide channel
liquidity.

## Proposed Solution

We propose to sequester the private keys and secrets in one or more
hardened policy signing devices.  We have a reference
[Validating Lightning Signer implementation](https://gitlab.com/lightning-signer/validating-lightning-signer) in Rust.
It currently has a gRPC interface, but other APIs are possible.

When run in external signing mode the Lightning node would use an alternate
signing module which replaces signing with proxy calls to the policy
signing devices.

The external signing device applies a complete set of policy controls
to ensure that the proposed transaction is safe to sign. Having a
[complete set of policy controls](policy-controls.md)
protects the funds even in the case of
a complete compromise of the node software. This will require some
overlap in logic between the node software and the policy signer.

<div align="center">
    <img src="./overview/system-overview.svg" width="700" height="500">
</div>

We also have a reference node implementation named [lnrod (Lightning Rod)](https://gitlab.com/lightning-signer/lnrod) in progress.


## Diagrams

#### Protocol Sequence Diagrams:
- [Channel Establishment Sequence](./seq-diagrams/channel-establishment.md)
- [Normal Operation Sequence](./seq-diagrams/normal-operation.md)
- [Dual Funding Sequence](./seq-diagrams/v2-channel-establishment.md)
- [Splicing Sequence](./seq-diagrams/splicing.md)

#### [Transaction Signing Diagrams](./tx-diagrams/README.md)

#### [Routing Policy Enforcement Walkthrough (PDF)](./routing-diagrams/partial-fail.pdf)

#### [System Details](./overview/README.md)

## Roadmap

The development of this approach has several distinct stages.  You
can see the [project roadmap here](roadmap.md).

## Chat

You can join https://matrix.to/#/#lightning-signer:libera.chat.

## Documents

* [Securing Lightning Nodes](https://medium.com/@devrandom/securing-lightning-nodes-39410747734b?)

* [Lightning Signing Policy Controls](policy-controls.md)


## Remote Lightning Signer Service

This is a reference implementation of the VLS.  The
current implementation is capable of complete external signing with
the [`c-lightning` remote signer
proxy](https://github.com/lightning-signer/c-lightning/pull/6).
Current development is now focused on policy enforcement and support
for additional lightning node implementations.

## Remote Signer API

See the [gRPC definition](https://gitlab.com/lightning-signer/validating-lightning-signer/-/blob/master/src/server/remotesigner.proto).

We also have preliminary support for the C-Lightning `hsmd` wire protocol.
