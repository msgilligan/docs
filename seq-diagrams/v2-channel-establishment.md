## V2 Channel Establishment Sequence (Dual-Funding_

[Dual Funding Spec](https://github.com/lightning/bolts/pull/851)

<br>

   | State Notation  | Signer State                                                     |
   |-----------------|------------------------------------------------------------------|
   | `H[] C[]`       | no holder commitment, no counterparty commitment                 |
   | `H[0] C[0]`     | holder initial commitment, counterparty initial commitment       |
   | `H[n] C[m]`     | holder commitment n, counterparty commitment m                   |
   | `H[n,n+1] C[m]` | holder commitment n and n+1, counterparty commitment m           |
   | `H[n]*`         | holder base commitment, splice, and RBF candidates for n         |
   | `H[n,n+1]*`     | holder base commitment, splice, and RBF candidates for n and n+1 |

<br>

```mermaid
sequenceDiagram
    autonumber

    participant Signer-A
    participant Node-A
    participant Node-B
    participant Signer-B

    Note over Node-A: Fund New Channel
    Node-A->>+Signer-A: NewChannel
    Signer-A-->>-Node-A: 

    Node-A->>Node-B: open_channel2

    Node-B->>+Signer-B: NewChannel
    Signer-B-->>-Node-B: 

    Node-B->>Node-A: accept_channel2

    Note over Node-A,Node-B: <tx collaboration>

    Node-A->>Node-B: <mutual> tx_complete

    Node-A->>+Signer-A: SetupChannel
    Signer-A-->>-Node-A: 
    Note over Signer-A: H[] C[]

    Node-B->>+Signer-B: SetupChannel
    Signer-B-->>-Node-B: 
    Note over Signer-B: H[] C[]

    Node-A->>+Signer-A: SignCounterpartyCommitment(0)
    Signer-A-->>-Node-A: 
    Note over Signer-A: H[] C[0]

    Node-A->>Node-B: commitment_signed(0)

    Node-B->>+Signer-B: ValidateHolderCommitment(0)
    Signer-B-->>-Node-B: 
    Note over Signer-B: H[0] C[]

    Node-B->>+Signer-B: SignCounterpartyCommitment(0)
    Signer-B-->>-Node-B: 
    Note over Signer-B: H[0] C[0]

    Node-B->>Node-A: commitment_signed(0)

    Node-A->>+Signer-A: ValidateHolderCommitment(0)
    Signer-A-->>-Node-A: 
    Note over Signer-A: H[0] C[0]

    Node-A ->>+Signer-A: SignOnchainTx
    Signer-A -->>-Node-A: 

    Node-A->>Node-B: tx_signatures

    Node-B ->>+Signer-B: SignOnchainTx
    Signer-B -->>-Node-B: 

    Node-B->>Node-A: tx_signatures


    opt
    	%% optional, pale blue
    	rect rgb(191, 223, 255)

            Note over Node-A: RBF Funding

            Node-A->>Node-B: tx_init_rbf
            Node-B->>Node-A: tx_ack_rbf

            Note over Node-A,Node-B: <tx rbf collaboration>

            Node-A->>Node-B: <mutual> tx_complete

            Node-A->>+Signer-A: SetupChannel
            Signer-A-->>-Node-A: 
            Note over Signer-A: H[0] C[0]

            Node-B->>+Signer-B: SetupChannel
            Signer-B-->>-Node-B: 
            Note over Signer-B: H[0] C[0]

            Node-A->>+Signer-A: SignCounterpartyCommitment(0')
            Signer-A-->>-Node-A: 
            Note over Signer-A: H[0] C[0]*

            Node-A->>Node-B: commitment_signed(0')

            Node-B->>+Signer-B: ValidateHolderCommitment(0')
            Signer-B-->>-Node-B: 
            Note over Signer-B: H[0]* C[0]

            Node-B->>+Signer-B: SignCounterpartyCommitment(0')
            Signer-B-->>-Node-B: 
            Note over Signer-B: H[0]* C[0]*

            Node-B->>Node-A: commitment_signed(0')

            Node-A->>+Signer-A: ValidateHolderCommitment(0')
            Signer-A-->>-Node-A: 
            Note over Signer-A: H[0]* C[0]*

            Node-A ->>+Signer-A: SignOnchainTx
            Signer-A -->>-Node-A: 

            Node-A->>Node-B: tx_signatures

            Node-B ->>+Signer-B: SignOnchainTx
            Signer-B -->>-Node-B: 

            Node-B->>Node-A: tx_signatures
        end
    end

    activate Node-A
    Note over Node-A: Funding Buried

    Note over Signer-A: H[0]* C[0]*
    Node-A ->>+Signer-A: CheckOutpoint(')
    Signer-A -->>-Node-A: 

    Node-A->>+Node-B: channel_ready(')

    Note over Signer-B: H[0]* C[0]*
    Node-B  ->>+Signer-B : CheckOutpoint(')
    Signer-B  -->>-Node-B : 

    Node-B  ->>+Signer-B : LockOutpoint(')
    Signer-B  -->>-Node-B : 

    Note over Signer-B: H[0'] C[0']

    Node-B->>Node-A: channel_ready(')

    deactivate Node-B
    Node-A  ->>+Signer-A : LockOutpoint(')
    Signer-A  -->>-Node-A : 
    Note over Signer-A: H[0'] C[0']
    deactivate Node-A
```
