## Splicing Sequence

[Splicing Spec](https://github.com/lightning/bolts/pull/863)

<br>

   | State Notation  | Signer State                                                     |
   |-----------------|------------------------------------------------------------------|
   | `H[] C[]`       | no holder commitment, no counterparty commitment                 |
   | `H[0] C[0]`     | holder initial commitment, counterparty initial commitment       |
   | `H[n] C[m]`     | holder commitment n, counterparty commitment m                   |
   | `H[n,n+1] C[m]` | holder commitment n and n+1, counterparty commitment m           |
   | `H[n]*`         | holder base commitment, splice, and RBF candidates for n         |
   | `H[n,n+1]*`     | holder base commitment, splice, and RBF candidates for n and n+1 |

<br>

```mermaid
sequenceDiagram
    autonumber

    participant Signer-A
    participant Node-A
    participant Node-B
    participant Signer-B

    Note over Node-A: Initiate Splice

    Node-A->>Node-B: splice

    Node-B->>Node-A: splice_ack

    Note over Node-A,Node-B: <tx collaboration>

    Node-A->>Node-B: <mutual> tx_complete

    Note over Signer-A: H[n] C[m]
    Node-A->>+Signer-A: SetupChannel
    Signer-A-->>-Node-A: 

    Note over Signer-B: H[m] C[n]
    Node-B->>+Signer-B: SetupChannel
    Signer-B-->>-Node-B: 

    Node-A->>+Signer-A: SignCounterpartyCommitment(m')
    Signer-A-->>-Node-A: 
    Note over Signer-A: H[n] C[m]*

    Node-A->>Node-B: commitment_signed(m')

    Node-B->>+Signer-B: ValidateHolderCommitment(m')
    Signer-B-->>-Node-B: 
    Note over Signer-B: H[m]* C[n]

    Node-B->>+Signer-B: SignCounterpartyCommitment(n')
    Signer-B-->>-Node-B: 
    Note over Signer-B: H[m]* C[n]*

    Node-B->>Node-A: commitment_signed(n')

    Node-A->>+Signer-A: ValidateHolderCommitment(n')
    Signer-A-->>-Node-A: 
    Note over Signer-A: H[n]* C[m]*

    Node-A ->>+Signer-A : SignOnchainTx
    Signer-A -->>-Node-A : 

    Node-A->>Node-B: tx_signatures

    Node-B ->>+Signer-B : SignOnchainTx
    Signer-B -->>-Node-B : 

    Node-B->>Node-A: tx_signatures

    opt
    	%% optional, pale blue
    	rect rgb(191, 223, 255)

            Note over Node-A: RBF Splice

            Node-A->>Node-B: tx_init_rbf
            Node-B->>Node-A: tx_ack_rbf

            Note over Node-A,Node-B: <splice rbf collaboration>

            Node-A->>Node-B: <mutual> tx_complete

            Note over Signer-A: H[n]* C[m]*
            Node-A->>+Signer-A: SetupChannel
            Signer-A-->>-Node-A: 

            Note over Signer-B: H[m]* C[n]*
            Node-B->>+Signer-B: SetupChannel
            Signer-B-->>-Node-B: 

            Node-A->>+Signer-A: SignCounterpartyCommitment(m'')
            Signer-A-->>-Node-A: 
            Note over Signer-A: H[n]* C[m]*

            Node-A->>Node-B: commitment_signed(m'')

            Node-B->>+Signer-B: ValidateHolderCommitment(m'')
            Signer-B-->>-Node-B: 
            Note over Signer-B: H[m]* C[n]*

            Node-B->>+Signer-B: SignCounterpartyCommitment(n'')
            Signer-B-->>-Node-B: 
            Note over Signer-B: H[m]* C[n]*

            Node-B->>Node-A: commitment_signed(n'')

            Node-A->>+Signer-A: ValidateHolderCommitment(n'')
            Signer-A-->>-Node-A: 
            Note over Signer-A: H[n]* C[m]*

            Node-A ->>+Signer-A : SignOnchainTx
            Signer-A -->>-Node-A : 

            Node-A->>Node-B: tx_signatures

            Node-B ->>+Signer-B : SignOnchainTx
            Signer-B -->>-Node-B : 

            Node-B->>Node-A: tx_signatures
        end
    end

    opt
    	%% optional, pale blue
    	rect rgb(191, 223, 255)

            Note over Node-A,Node-B: Operation Before Locked

            Note over Node-A,Node-B: <calls to update_add_htlc>

            par
    	        %% Node-A initiated, soft purple
    	        rect rgb(204, 183, 223)

                    activate Node-A
                    Note over Node-A: Update Node B's Commitment

                    Note over Signer-A: H[n]* C[m]*
                    Node-A->>+Signer-A: SignCounterpartyCommitment(m+1)
                    Signer-A-->>-Node-A: 
                    Note over Signer-A: H[n]* C[m,m+1]*

                    Node-A->>+Node-B: commitment_signed(m+1)

            	    Note over Signer-B: H[m]* C[n]*
                    Node-B->>+Signer-B: ValidateHolderCommitment(m+1)
                    Signer-B-->>-Node-B: 
                    Note over Signer-B: H[m,m+1]* C[n]*

                    Node-A->>+Signer-A: SignCounterpartyCommitment(m+1')
                    Signer-A-->>-Node-A: 

                    Node-A->>Node-B: commitment_signed(m+1')

                    Node-B->>+Signer-B: ValidateHolderCommitment(m+1')
                    Signer-B-->>-Node-B: 

                    Node-A->>+Signer-A: SignCounterpartyCommitment(m+1'')
                    Signer-A-->>-Node-A: 

                    Node-A->>Node-B: commitment_signed(m+1'')

                    Node-B->>+Signer-B: ValidateHolderCommitment(m+1'')
                    Signer-B-->>-Node-B: 

                    Note over Node-B: persist H[m+1]*

                    Node-B->>+Signer-B: RevokeHolderCommitment(m)
                    Signer-B-->>-Node-B: 
                    Note over Signer-B: H[m+1]* C[n]*

                    Node-B->>Node-A: revoke_and_ack(m)

                    deactivate Node-B
                    Node-A->>+Signer-A: ValidateCounterpartyRevocation(m)
                    Signer-A-->>-Node-A: 
            	    Note over Signer-A: H[n]* C[m+1]*
                    deactivate Node-A

                end
            and
               	%% Node-B initiated, soft teal
               	rect rgb(143, 188, 187)

                    activate Node-B
                    Note over Node-B: Update Node A's Commitment

                    Note over Signer-B: H[m]* C[n]*
                    Node-B->>+Signer-B: SignCounterpartyCommitment(n+1)
                    Signer-B-->>-Node-B: 
                    Note over Signer-B: H[m]* C[n,n+1]*

                    Node-B->>+Node-A: commitment_signed(n+1)

            	    Note over Signer-A: H[n]* C[m]*
                    Node-A->>+Signer-A: ValidateHolderCommitment(n+1)
                    Signer-A-->>-Node-A: 
                    Note over Signer-A: H[n,n+1]* C[m]*

                    Node-B->>+Signer-B: SignCounterpartyCommitment(n+1')
                    Signer-B-->>-Node-B: 

                    Node-B->>Node-A: commitment_signed(n+1')

                    Node-A->>+Signer-A: ValidateHolderCommitment(n+1')
                    Signer-A-->>-Node-A: 

                    Node-B->>+Signer-B: SignCounterpartyCommitment(n+1'')
                    Signer-B-->>-Node-B: 

                    Node-B->>Node-A: commitment_signed(n+1'')

                    Node-A->>+Signer-A: ValidateHolderCommitment(n+1'')
                    Signer-A-->>-Node-A: 

                    Note over Node-A: persist H[n+1]*

                    Node-A->>+Signer-A: RevokeHolderCommitment(n)
                    Signer-A-->>-Node-A: 
                    Note over Signer-A: H[n+1]* C[m]*

                    Node-A->>Node-B: revoke_and_ack(n)

                    deactivate Node-A
                    Node-B->>+Signer-B: ValidateCounterpartyRevocation(n)
                    Signer-B-->>-Node-B: 
                    Note over Signer-B: H[m]* C[n+1]*
                    deactivate Node-B
                end
            end
        end
    end

    activate Node-A
    Note over Node-A: Splice Locked

    Note over Signer-A: H[n]* C[m]*
    Node-A ->>+Signer-A : CheckOutpoint('')
    Signer-A -->>-Node-A : 

    Node-A->>+Node-B: splice_locked('')

    Note over Signer-B: H[m]* C[n]*
    Node-B  ->>+Signer-B  : CheckOutpoint('')
    Signer-B  -->>-Node-B  : 

    Node-B  ->>+Signer-B  : LockOutpoint('')
    Signer-B  -->>-Node-B  : 
    Note over Signer-B: H[m''] C[n'']

    Node-B->>Node-A: splice_locked('')

    deactivate Node-B
    Node-A  ->>+Signer-A  : LockOutpoint('')
    Signer-A  -->>-Node-A  : 
    Note over Signer-A: H[n''] C[m'']
    deactivate Node-A
```
