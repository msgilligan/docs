### [Back to Lightning-Signer Home](README.md)

Lightning Signer Project Roadmap
================================================================

### Achieve Consensus for a Signing API and Drive Adoption

Ideally the same external signing daemon could be used for all of the
major lightning node implementations.

### External Signing for Lightning Node Implementations

Add an external signing mode in existing lightning node
implementations with proxied calls to an external lightning signing
daemon.
In that mode, no private keys/secrets exist in the
lightning node itself.

### Policy Enforcement for Signed Transactions

All signing operations in the external signing daemon are subjected to a
comprehensive set of policy rules which ensure that an attacker cannot
maliciously divert funds.

### State Backups, Node Recovery Procedures

The external signing daemon creates robust and secure state
backups which can be used to restore its operational state.

### UTXO Set Oracle

A specific challenge is providing a succinct proof to the signer that a
particular UTXO is unspent at the current time.
By using a set of UTXO oracle servers which sign the root of a merkle
tree containing all current unspent UTXOs, compact proofs can be provided
to the external signing daemon.

### Security Enhanced Hardware

The external signing daemon is implemented on hardware enhanced
hardware.
A hardware security module (HSM), a secure element (SE), a
smart card, a trusted execution environment (TEE) and a secure enclave
(SE again) are all examples of possible enhanced environments for
secure execution.

### Multi-Party Signing

In this type of deployment, the external signer is implemented as
a cooperating set of separate daemons using multi-party computation
to sign transaction requests.
Since none of the separate daemons has a full private keys/secrets an
attacker would need to compromise a quorum of the individual daemons.
